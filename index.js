/*
3. Create a variable number that will store the value of the number provided by the user via the prompt.
4. Create a condition that if the current value is less than or equal to 50, stop the loop.
5. Create another condition that if the current value is divisible by 10, print a message that the number is being skipped and continue to the next iteration of the loop.
6. Create another condition that if the current value is divisible by 5, print the number.
*/

let num = Number(prompt("Give me a number: "));
console.log("The number you provided is: " + num);
for(let input = num ; input > 50 ; input--){
	if(input % 10 === 0){
		console.log("The number is divisible by 10. Number is being skipped.");
		continue;
	}

	if(input % 5 === 0){
		console.log(input);
		continue;
	}

	if(input <= 50){
		break;
	}

}	
console.log("The current value is less than or equal to 50, stop the loop.");

/*
7. Create a variable that will contain the string supercalifragilisticexpialidocious.
8. Create another variable that will store the consonants from the string.
9. Create another for Loop that will iterate through the individual letters of the string based on it’s length.
10. Create an if statement that will check if the letter of the string is equal to a vowel and continue to the next iteration of the loop if it is true.
11. Create an else statement that will add the letter to the second variable.
*/

let var1 = "supercalifragilisticexpialidocious";
let var2 = "";

console.log(var1);

for(i = 0; i < var1.length; i++){


	if(
		var1[i].toLowerCase() == "a" ||
		var1[i].toLowerCase() == "e" ||
		var1[i].toLowerCase() == "i" ||
		var1[i].toLowerCase() == "o" ||
		var1[i].toLowerCase() == "u" 
		){
		continue;
	}

	else{

		var2 += var1[i];

	}

	console.log()
	if(i > var1.length){
		break;
	}
}
console.log(var2);
